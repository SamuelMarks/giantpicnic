# -*- coding: utf-8 -*-

if 0: # Stuff for Eclipse
    from gluon.languages import translator as T

    global db

    from gluon.tools import Auth
    auth = Auth(db, hmac_key=Auth.get_or_create_key())

    from gluon.globals import Response
    response = Response()

@auth.requires_login()
def index():
    mygroups = "Sign up to some groups!"
    for group_id in auth.user.member_of:
        mygroups = db(db.picnics.id == group_id).select() 
    user_profile = dict(email=auth.user.email, name=auth.user.name, member_of=mygroups,
                        rsvp_yes=auth.user.rsvp_yes, rsvp_maybe=auth.user.rsvp_maybe, interests=auth.user.interests)
    return dict(user_profile=user_profile)
